<?php


namespace App\Actions\Pets;


use App\Models\Pet;

class CreatePetAction
{
    private Pet $pet;

    private CreateTreatmentAction $createTreatmentAction;

    public function __construct(
        Pet $pet,
        CreateTreatmentAction $createTreatmentAction
    )
    {
        $this->pet = $pet;
        $this->createTreatmentAction = $createTreatmentAction;
    }

    public function execute(array $validData): Pet
    {
        $pet = $this->pet->create(
            [
                'name' => $validData['name'],
                'species' => $validData['species'],
            ]
        );

        $this->createTreatmentAction->execute(
            $pet,
            [
                'description' => $validData['description'],
                'treated_at' => $validData['treated_at'],
            ]
        );

        return $pet;
    }
}
