<?php
namespace App\Actions\Pets;

use App\Models\Pet;

class CreateTreatmentAction
{
    public function execute(Pet $pet, array $data)
    {
        return $pet->treatments()
            ->create(
                $data
            );
    }
}
