<?php
namespace App\Repositories;

use App\Models\Pet;
use Illuminate\Pagination\LengthAwarePaginator;

class PetsRepository
{
    private $pet;

    public function __construct(Pet $pet)
    {
        $this->pet = $pet;
    }

    public function getWithTreatmentsOrderByOldest(
        bool $hasFilter = null,
        string $filter = null
    ): LengthAwarePaginator
    {
        return $this->pet
            ->oldest()
            ->when(
                $hasFilter,
                function($query) use($filter) {
                    $query->where('name', 'LIKE', '%'.$filter.'%');
                }
            )
            ->with('treatments')
            ->paginate();
    }
}
