<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Treatment extends Model
{
    protected $fillable = [
        'description',
        'treated_at',
        'pet_id',
    ];

    protected $dates = [
        'treated_at'
    ];

    public function pet()
    {
        return $this->belongsTo(Pet::class);
    }
}
