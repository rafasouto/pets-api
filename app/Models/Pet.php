<?php

namespace App\Models;

use App\Repositories\PetsRepository;
use Illuminate\Database\Eloquent\Model;

class Pet extends Model
{
    protected $fillable = [
        'name', 'species'
    ];

    public static function getRepository()
    {
        return new PetsRepository(new self);
    }

    public function treatments()
    {
        return $this->hasMany(Treatment::class);
    }

    public function getSpeciesParsedAttribute()
    {
        if ($this->species == 'C') {
            return 'cão';
        }

        if ($this->species == 'G') {
            return 'gato';
        }
    }

    public function getCapitalNameAttribute()
    {
        return ucfirst($this->name);
    }
}
