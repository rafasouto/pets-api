<?php

namespace App\Console\Commands;

use App\Models\Pet;
use Illuminate\Console\Command;

class OutputExampleCommand extends Command
{
    protected $signature = 'echo:example';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $pet = factory(Pet::class)
            ->create();

        $treatment = $pet->treatments->first();

        $this->info("Em {$treatment->treated_at->format('d/m/Y')} o pet {$pet->capital_name} ($pet->species_parsed) '{$treatment->description}'");
    }
}
