<?php

namespace App\Http\Controllers\Pets;

use App\Actions\Pets\CreateTreatmentAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\Pets\TreatmentRequest;
use App\Http\Responses\CreatedResponse;
use App\Models\Pet;
use Illuminate\Http\Request;

class AttachTreatmentController extends Controller
{
    private CreateTreatmentAction $createTreatmentAction;

    public function __construct(CreateTreatmentAction $createTreatmentAction)
    {
        $this->createTreatmentAction = $createTreatmentAction;
    }

    public function __invoke(
        Pet $pet,
        TreatmentRequest $request
    )
    {
        $content = array_merge(
            $request->validated(),
            [
                'pet_id' => $pet->id
            ]
        );

        $this->createTreatmentAction->execute($pet, $content);

        return new CreatedResponse($pet->load('treatments'));
    }
}
