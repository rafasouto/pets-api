<?php

namespace App\Http\Controllers\Pets;

use App\Actions\Pets\CreatePetAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\Pets\CreatePetRequest;
use App\Http\Responses\CreatedResponse;
use App\Http\Responses\NoContentResponse;
use App\Http\Responses\SuccessfulResponse;
use App\Models\Pet;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PetsController extends Controller
{
    public function index(Request $request): JsonResponse
    {
        $pets = Pet::getRepository()
            ->getWithTreatmentsOrderByOldest(
                $request->has('filter'),
                $request->get('filter')
            );

        return new SuccessfulResponse($pets);
    }

    public function store(
        CreatePetRequest $request,
        CreatePetAction $createPetAction
    ): JsonResponse
    {
        $pet = $createPetAction->execute($request->validated());

        return new CreatedResponse($pet->load('treatments'));
    }

    public function destroy(Pet $pet): JsonResponse
    {
        $pet->delete();

        return new NoContentResponse();
    }
}
