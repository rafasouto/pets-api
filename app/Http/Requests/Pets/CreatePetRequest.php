<?php

namespace App\Http\Requests\Pets;

use App\Http\Responses\UnauthorizedResponse;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreatePetRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check() ? true : new UnauthorizedResponse();
    }

    public function rules()
    {
        return [
            'description' => 'sometimes',
            'treated_at' => 'required|date',
            'name' => 'required|min:2',
            'species' => [
                Rule::in(['C', 'G']),
            ]
        ];
    }
}
