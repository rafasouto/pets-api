<?php

namespace App\Http\Requests\Pets;

use App\Http\Responses\UnauthorizedResponse;
use Illuminate\Foundation\Http\FormRequest;

class TreatmentRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check() ? true : new UnauthorizedResponse();
    }

    public function rules()
    {
        return [
            'description' => 'required',
            'treated_at' => 'required|date',
        ];
    }
}
