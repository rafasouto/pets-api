<?php
namespace App\Http\Responses;

use Illuminate\Http\JsonResponse;

class NoContentResponse extends JsonResponse
{
    private int $code = 204;

    public function __construct()
    {
        parent::__construct(
            [],
            $this->code
        );
    }

}
