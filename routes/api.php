<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Pets\AttachTreatmentController;
use App\Http\Controllers\Pets\PetsController;
use Illuminate\Support\Facades\Route;

Route::post('login', LoginController::class);

Route::group([
    'middleware' => 'auth:api',
], function () {
    Route::post('register', RegisterController::class);
    Route::post('/pet', [PetsController::class, 'store']);
    Route::get('/pet', [PetsController::class, 'index']);
    Route::delete('/pet/{pet}', [PetsController::class, 'destroy']);
    Route::post('/treatment/{pet}', AttachTreatmentController::class);
});
