## No Faro pet api

Esse repositório é a resolução do teste proposto de uma API para gerenciamento de pets e suas consultas.

Tecnologias:

- PHP 7.4
- Mysql 5.7
- Docker
- Laravel 7

Em adicional foram feitos endpoints para autenticação via JWT para os recursos não ficarem expostos. Será criado através das migrations um usuário default com as credenciais:

email: nofaro@gmail.com
senha: 123456

É necessário gerar um token de autenticação em `/login` e adicioná-lo nas requests subsequents no header `Authorization`.

Todo teste foi desenvolvido utilizando a metodologia TDD, a suíte de testes pode ser encontrada em `/tests/features`.

### Endpoints

https://documenter.getpostman.com/view/4615609/T17FCUvL

