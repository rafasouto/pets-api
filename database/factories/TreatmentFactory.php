<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Treatment;
use Faker\Generator as Faker;

$factory->define(Treatment::class, function (Faker $faker) {
    return [
        'description' => $faker->sentence,
        'treated_at' => $faker->date(),
    ];
});
