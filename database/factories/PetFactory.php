<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Pet;
use App\Models\Treatment;
use Faker\Generator as Faker;

$factory->define(Pet::class, function (Faker $faker) {
    $species = ['G', 'C'];
    $randomSpecie = array_rand($species);
    return [
        'name' => $faker->name(),
        'species' => $species[$randomSpecie],
    ];
});

$factory->afterCreating(Pet::class, function ($pet) {
    $treatment = factory(Treatment::class)->make();

    $treatment->pet()
        ->associate($pet)
        ->save();
});
