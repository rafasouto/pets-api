<?php

namespace Tests\Feature\Pet;

use App\Models\Pet;
use App\Models\Treatment;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PetTest extends TestCase
{
    use RefreshDatabase;

    public function testShouldCreateAPet()
    {
        $this->withoutExceptionHandling();

        $pet = factory(Pet::class)
            ->make()
            ->toArray();

        $treatment =  factory(Treatment::class)
            ->make()
            ->toArray();

        $content = array_merge(
            $pet,
            $treatment
        );

        $token = $this->getAuthenticatedUserToken();

        $response = $this->post(
            '/api/pet',
            $content,
            [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $token"
            ]
        );

        $response->assertCreated();

        $this->assertDatabaseHas(
            'pets',
            [
                'name' => $content['name'],
                'species' => $content['species'],
            ]
        );

        $this->assertDatabaseHas(
            'treatments',
            [
                'description' => $content['description'],
                'treated_at' => $content['treated_at'],
            ]
        );
    }

    public function testShouldRequireAName()
    {
        $pet = factory(Pet::class)
            ->make(['name' => ''])
            ->toArray();

        $treatment =  factory(Treatment::class)
            ->make()
            ->toArray();

        $content = array_merge(
            $pet,
            $treatment
        );

        $token = $this->getAuthenticatedUserToken();

        $response = $this->post(
            '/api/pet',
            $content,
            [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $token"
            ]
        );

        $response->assertJsonValidationErrors('name');
    }

    public function testShouldNameHaveAMinimumLengthOfTwo()
    {
        $pet = factory(Pet::class)
            ->make(['name' => 'R'])
            ->toArray();

        $treatment =  factory(Treatment::class)
            ->make()
            ->toArray();

        $content = array_merge(
            $pet,
            $treatment
        );

        $token = $this->getAuthenticatedUserToken();

        $response = $this->post(
            '/api/pet',
            $content,
            [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $token"
            ]
        );

        $response->assertJsonValidationErrors('name');
    }

    public function testShouldRequireASpecies()
    {
        $pet = factory(Pet::class)
            ->make(['species' => ''])
            ->toArray();

        $treatment = factory(Treatment::class)
            ->make()
            ->toArray();

        $content = array_merge(
            $pet,
            $treatment
        );

        $token = $this->getAuthenticatedUserToken();

        $response = $this->post(
            '/api/pet',
            $content,
            [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $token"
            ]
        );

        $response->assertJsonValidationErrors('species');
    }

    public function testSpeciesShouldHaveCOrGValue()
    {
        $pet = factory(Pet::class)
            ->make(['species' => 'Q'])
            ->toArray();

        $treatment = factory(Treatment::class)
            ->make()
            ->toArray();

        $content = array_merge(
            $pet,
            $treatment
        );

        $token = $this->getAuthenticatedUserToken();

        $response = $this->post(
            '/api/pet',
            $content,
            [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $token"
            ]
        );

        $response->assertJsonValidationErrors('species');
    }

    public function testTreatmentShouldHaveTreatedAt()
    {
        $pet = factory(Pet::class)
            ->make()
            ->toArray();

        $treatment = factory(Treatment::class)
            ->make(['treated_at' => ''])
            ->toArray();

        $content = array_merge(
            $pet,
            $treatment
        );

        $token = $this->getAuthenticatedUserToken();

        $response = $this->post(
            '/api/pet',
            $content,
            [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $token"
            ]
        );

        $response->assertJsonValidationErrors('treated_at');
    }


    public function testTreatmentTreatedAtShouldBeADate()
    {
        $pet = factory(Pet::class)
            ->make()
            ->toArray();

        $treatment = factory(Treatment::class)
            ->make(['treated_at' => 'saijijds'])
            ->toArray();

        $content = array_merge(
            $pet,
            $treatment
        );

        $token = $this->getAuthenticatedUserToken();

        $response = $this->post(
            '/api/pet',
            $content,
            [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $token"
            ]
        );

        $response->assertJsonValidationErrors('treated_at');
    }

    public function testShouldListsAllPets()
    {
        $this->withoutExceptionHandling();

        $pet = factory(Pet::class)
            ->create();

        $token = $this->getAuthenticatedUserToken();

        $response = $this->get(
            '/api/pet',
            [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $token"
            ]
        );

        $response->assertOk();

        $response->assertJsonFragment(
            $pet->toArray()
        );
    }

    public function testShouldFilterAPet()
    {
        $this->withoutExceptionHandling();

        $pet = factory(Pet::class)
            ->create(
                [
                    'name' => 'Schrodinger',
                    'species' => 'C'
                ]
            );

        $token = $this->getAuthenticatedUserToken();

        $response = $this->get(
            '/api/pet?filter=Schrodinger',
            [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $token"
            ]
        );

        $response->assertOk();

        $response->assertJsonFragment(
            $pet->toArray()
        );
    }

    public function testShouldAttachTreatment()
    {
        $this->withoutExceptionHandling();

        $pet = factory(Pet::class)->create();

        $treatment = factory(Treatment::class)
            ->make()
            ->toArray();

        $token = $this->getAuthenticatedUserToken();

        $response = $this->post(
            '/api/treatment/'.$pet->id,
            $treatment,
            [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $token"
            ]
        );

        $response->assertCreated();

        $this->assertDatabaseHas(
            'treatments',
            $treatment
        );
    }

    public function testShouldDeleteAPet()
    {
        $this->withoutExceptionHandling();

        $pet = factory(Pet::class)->create();

        $token = $this->getAuthenticatedUserToken();

        $response = $this->delete(
            '/api/pet/'.$pet->id,
            [],
            [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $token"
            ]
        );

        $response->assertNoContent();

        $this->assertDatabaseMissing(
            'pets',
            $pet->toArray()
        );

        $this->assertDatabaseMissing(
            'treatments',
            $pet->treatments->toArray()
        );
    }
}
