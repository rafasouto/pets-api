<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RegisterUserTest extends TestCase
{
    use RefreshDatabase;

    public function testShouldRegisterAUser()
    {
        $this->withoutExceptionHandling();

        $user = factory(User::class)
            ->make()
            ->toArray();

        $token = $this->getAuthenticatedUserToken();

        $response = $this->post(
            '/api/register',
            array_merge($user, ['password' => 123456]),
            [
                'accept' => 'application/json',
                'Authorization' => "Bearer $token"
            ]
        );

        $response->assertCreated();

        $this->assertDatabaseHas(
            'users',
            [
                'name' => $user['name'],
                'email' => $user['email'],
            ]
        );
    }

    public function testShouldValidateName()
    {
        $user = factory(User::class)
            ->make(['name' => ''])
            ->toArray();

        $token = $this->getAuthenticatedUserToken();

        $response = $this->post(
            '/api/register',
            array_merge($user, ['password' => 123456]),
            [
                'accept' => 'application/json',
                'Authorization' => "Bearer $token"
            ]
        );

        $response->assertJsonValidationErrors('name');
        $response->assertStatus(422);
    }

    public function testShouldValidateEmail()
    {
        $user = factory(User::class)
            ->make(['email' => ''])
            ->toArray();

        $token = $this->getAuthenticatedUserToken();

        $response = $this->post(
            '/api/register',
            array_merge($user, ['password' => 123456]),
            [
                'accept' => 'application/json',
                'Authorization' => "Bearer $token"
            ]
        );

        $response->assertJsonValidationErrors('email');
        $response->assertStatus(422);
    }

    public function testShouldValidatePassword()
    {
        $user = factory(User::class)
            ->make()
            ->toArray();

        $token = $this->getAuthenticatedUserToken();

        $response = $this->post(
            '/api/register',
            $user,
            [
                'accept' => 'application/json',
                'Authorization' => "Bearer $token"
            ]
        );

        $response->assertJsonValidationErrors('password');
        $response->assertStatus(422);
    }
}
