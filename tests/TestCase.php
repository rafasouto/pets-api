<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function getAuthenticatedUserToken(): string
    {
        $user = factory(User::class)->create();

        return JWTAuth::fromUser($user);
    }
}
